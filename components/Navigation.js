import data from "../model/Model.json";
import Link from "next/link";

import { FaInstagram, FaGoogle, FaFacebookSquare } from "react-icons/fa";
import { GiHamburgerMenu } from "react-icons/gi";
import { VscMenu } from "react-icons/vsc";

import { useState } from "react";

export default function Navigation() {
  const pages = data.pages;
  const [toggle, toggleNavbar] = useState(false);

  const disableScroll = function () {
    if (!toggle) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "scroll";
    }
  };
  return (
    <div className="flex flex-row bg-[#020b15] justify-between fixed z-50 top-0 h-[64px] items-center w-full px-3">
      <div className="font-main-regular text-[1.5rem] items-center  text-secondary">
        <a href="/">
          <p className="ml-2 tracking-[-0.2rem] c4:text-[1.9rem] font-black">
            STAKEBROS.
          </p>
        </a>
      </div>
      <div className=" flex flex-row font-main-regular  items-center justify-center ">
        {pages.map((page, index) => (
          <div key={index}>
            {!page.sub_page && page.icon === "" && page.title && (
              <a href={page.href}>
                <p className="mx-4 font-bold uppercase text-secondary c4:text-[1.3rem]">
                  {page.title}
                </p>
              </a>
            )}
          </div>
        ))}
      </div>
    </div>
  );
}
