import { HiOutlineArrowNarrowDown } from "react-icons/hi";
export default function Claim(props) {
  return (
    <main>
      <div className="relative top-[-64px] h-screen w-full font-main-regular italic flex flex-col items-center justify-center">
        {props.data.claim.split("<br/>").map((item, i) => (
          <div key={i}>
            <p className="text-[3rem] c6:text-[5rem] c6:leading-[4rem] leading-[2rem] font-thin text-center text-white ">
              {item}{" "}
              {props.data.claim.split("<br/>").length === i + 1 && (
                <span className="text-secondary relative top-[3px] -left-1 ">
                  .
                </span>
              )}
            </p>
            <br />
          </div>
        ))}
        <a href={props.data.anchor_link} className="flex flex-col items-center">
          <p className="text-secondary text-[3rem] relative top-20 bounce-arrow">
            <HiOutlineArrowNarrowDown />
          </p>
          <p className="mx-4 font-light text-secondary relative top-28 text-[1.5rem] blink-text">
            {props.data.anchor_name}
          </p>
        </a>
      </div>
    </main>
  );
}
