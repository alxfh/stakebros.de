export default function Imprint() {
  return (
    <div
      className="imprint mb-36 mt-20 md:mt-8 xl:mt-24 mx-auto text-white font-main-regular main-padding
                    lg:max-w-[80vw] c1xl:max-w-[75vw] c2xl:max-w-[65vw] c3xl:max-w-[55vw] c4xl:max-w-[65vw]"
    >
      <h1>Impressum</h1>

      <h2>Angaben gemäß § 5 TMG</h2>
      <p>
        Kunden Name
        <br />
        Kunden Name Agentur
        <br />
        Kundenstraße 29
        <br />
        99867 Kundenstadt
      </p>

      <h2>Kontakt</h2>
      <p>
        Telefon: +49 (0) 1234 56789
        <br />
        Telefax: +49 (0) 1234 56789
        <br />
        E-Mail: kontakt@KundenName.de
      </p>

      <h2>Umsatzsteuer-ID</h2>
      <p>
        Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz:
        <br />
        DE313070114
      </p>

      <h2>Redaktionell verantwortlich</h2>
      <p>
        Kunden Name
        <br />
        Goldbacher Straße 29
        <br />
        99867 Gotha
      </p>

      <h2>EU-Streitschlichtung</h2>
      <p>
        Die Europäische Kommission stellt eine Plattform zur
        Online-Streitbeilegung (OS) bereit:{" "}
        <a
          href="https://ec.europa.eu/consumers/odr/"
          target="_blank"
          rel="noopener noreferrer"
        >
          https://ec.europa.eu/consumers/odr/
        </a>
        .<br /> Unsere E-Mail-Adresse finden Sie oben im Impressum.
      </p>

      <h2>Verbraucher­streit­beilegung/Universal­schlichtungs­stelle</h2>
      <p>
        Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren
        vor einer Verbraucherschlichtungsstelle teilzunehmen.
      </p>
    </div>
  );
}
