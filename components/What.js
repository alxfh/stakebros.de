import { useEffect, useState } from "react";
import { HiOutlineArrowNarrowDown } from "react-icons/hi";

export default function What() {
  const rebase = 8;
  const [isLoading, setIsLoading] = useState(true);

  const [data, setData] = useState([
    {
      name: "Snowbank",
      price: 0,
      roi_1day: 0.7561,
      currency: "$",
      img: "/snowbank.png",
      api: "snowbank",
    },
    {
      name: "Wonderland",
      price: 0,
      roi_1day: 0.6155,
      currency: "$",
      img: "/wonderland.png",
      api: "wonderland",
    },
    {
      name: "Snowdog",
      price: 0,
      roi_1day: 1.4106,
      currency: "$",
      img: "/snowdog.png",
      api: "snowdog",
    },
    {
      name: "Spartacus",
      price: 0,
      roi_1day: 0.9205,
      currency: "$",
      img: "/spartacus.png",
      api: "spartacus",
    },
    {
      name: "Hector",
      price: 0,
      roi_1day: 0.7953,
      currency: "$",
      img: "/hector.webp",
      api: "hector-dao",
    },
  ]);

  const [profit, setProfit] = useState([
    {
      profit_day: 0,
      profit_week: 0,
      profit_month: 0,
      profit_year: 0,
    },
    {
      profit_day: 0,
      profit_week: 0,
      profit_month: 0,
      profit_year: 0,
    },
    {
      profit_day: 0,
      profit_week: 0,
      profit_month: 0,
      profit_year: 0,
    },
    {
      profit_day: 0,
      profit_week: 0,
      profit_month: 0,
      profit_year: 0,
    },
    {
      profit_day: 0,
      profit_week: 0,
      profit_month: 0,
      profit_year: 0,
    },
  ]);

  const [toSend, setToSend] = useState({
    invest: "",
  });

  const profitCalculator = (invest, roi, totalHours) => {
    const loops = totalHours / rebase;
    let profit = parseInt(invest);
    for (let i = 0; i < loops; i++) {
      profit += profit * (roi / 100);
    }
    console.log("Stake:", profit);
    return (profit - parseInt(invest)).toFixed(2);
  };

  const handleChange = (e) => {
    setToSend({ ...toSend, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    if (toSend.invest !== "") {
      for (var k = 0; k < profit.length; k++) {
        //day
        profit[k].profit_day = profitCalculator(
          toSend.invest,
          data[k].roi_1day,
          24
        );
        //week
        profit[k].profit_week = profitCalculator(
          toSend.invest,
          data[k].roi_1day,
          24 * 7
        );
        //month
        profit[k].profit_month = profitCalculator(
          toSend.invest,
          data[k].roi_1day,
          24 * 30
        );
        //year
        profit[k].profit_year = profitCalculator(
          toSend.invest,
          data[k].roi_1day,
          24 * 365
        );
        //lambo
        //profit[k][l].profit_lambo = profitCalculator(100, 10, 24);}
      }
      setProfit([...profit]);
    }
  }, [toSend.invest]);

  /* 
  //day
  profitCalculator(100, 10, 24);
  //week
  profitCalculator(100, 10, 24 * 7);
  //month
  profitCalculator(100, 10, 24 * 30);
  //year
  profitCalculator(100, 10, 24 * 365); 
  */

  useEffect(() => {
    const wrapper = async () => {
      let coins = "";
      for (var i = 0; i < data.length; i++) {
        if (i === data.length - 1) {
          coins += data[i].api;
        } else {
          coins += data[i].api + ",";
        }
      }
      let url =
        "https://api.coingecko.com/api/v3/simple/price?ids=" +
        coins +
        "&vs_currencies=usd";
      const res = await fetch(url);
      const coinsData = await res.json();

      let urlHistory =
        "https://api.coingecko.com/api/v3/coins/spartacus/market_chart?vs_currency=usd&days=7&interval=1m";
      const resHistory = await fetch(urlHistory);
      const history = await resHistory.json();
      console.log(history);
      let historyData = { index: [], price: [], volumes: [] };
      for (const item of history.prices) {
        historyData.index.push(item[0]);
        historyData.price.push(item[1]);
      }
      for (const item of history.total_volumes)
        historyData.volumes.push(item[1]);

      let urlHistoryHector =
        "https://api.coingecko.com/api/v3/coins/hector-dao/market_chart?vs_currency=usd&days=7&interval=1m";
      const resHistoryHector = await fetch(urlHistoryHector);
      const historyHector = await resHistoryHector.json();

      let historyHectorData = { index: [], price: [], volumes: [] };
      for (const item of historyHector.prices) {
        historyHectorData.index.push(item[0]);
        historyHectorData.price.push(item[1]);
      }
      for (const item of historyHector.total_volumes)
        historyHectorData.volumes.push(item[1]);

      let urlHistorySnow =
        "https://api.coingecko.com/api/v3/coins/snowbank/market_chart?vs_currency=usd&days=7&interval=1m";
      const resHistorySnow = await fetch(urlHistorySnow);
      const historySnow = await resHistorySnow.json();

      let historySnowData = { index: [], price: [], volumes: [] };
      for (const item of historySnow.prices) {
        historySnowData.index.push(item[0]);
        historySnowData.price.push(item[1] / 10);
      }
      for (const item of historySnow.total_volumes)
        historySnowData.volumes.push(item[1]);

      let urlHistoryWonderland =
        "https://api.coingecko.com/api/v3/coins/wonderland/market_chart?vs_currency=usd&days=7&interval=1m";
      const resHistoryWonderland = await fetch(urlHistoryWonderland);
      const historyWonderland = await resHistoryWonderland.json();

      let historyWonderlandData = { index: [], price: [], volumes: [] };
      for (const item of historyWonderland.prices) {
        historyWonderlandData.index.push(item[0]);
        historyWonderlandData.price.push(item[1] / 15);
      }
      for (const item of historyWonderland.total_volumes)
        historyWonderlandData.volumes.push(item[1]);

      let urlHistorySnowdog =
        "https://api.coingecko.com/api/v3/coins/snowdog/market_chart?vs_currency=usd&days=7&interval=1m";
      const resHistorySnowdog = await fetch(urlHistorySnowdog);
      const historySnowdog = await resHistorySnowdog.json();

      let historySnowdogData = { index: [], price: [], volumes: [] };
      for (const item of historySnowdog.prices) {
        historySnowdogData.index.push(item[0]);
        historySnowdogData.price.push(item[1] / 10);
      }
      for (const item of historySnowdog.total_volumes)
        historySnowdogData.volumes.push(item[1]);

      let urlHistoryAvax =
        "https://api.coingecko.com/api/v3/coins/avalanche-2/market_chart?vs_currency=usd&days=7&interval=1m";
      const resHistoryAvax = await fetch(urlHistoryAvax);
      const historyAvax = await resHistoryAvax.json();

      let historyAvaxData = { index: [], price: [], volumes: [] };
      for (const item of historyAvax.prices) {
        historyAvaxData.index.push(item[0]);
        historyAvaxData.price.push(item[1] * 4);
      }
      for (const item of historyAvax.total_volumes)
        historyAvaxData.volumes.push(item[1]);

      setIsLoading(false);
      const initChart = (
        data,
        dataHector,
        dataSnowbank,
        dataWonderland,
        dataSnowdog,
        dataAvax
      ) => {
        let trace_price = {
          name: "Price Spartacus($)",
          x: data.index.map((t) => new Date(t)),
          y: data.price,
          xaxis: "x",
          yaxis: "y1",
          type: "scatter",
          mode: "lines+markers",
          marker: { color: "blue", size: 3 },
        };
        let trace_price_2nd = {
          name: "Price Hector($)",
          x: dataHector.index.map((t) => new Date(t)),
          y: dataHector.price,
          xaxis: "x",
          yaxis: "y1",
          type: "scatter",
          mode: "lines+markers",
          marker: { color: "purple", size: 5 },
        };
        let trace_price_3rd = {
          name: "Price Snowbank($)",
          x: dataSnowbank.index.map((t) => new Date(t)),
          y: dataSnowbank.price,
          xaxis: "x",
          yaxis: "y1",
          type: "scatter",
          mode: "lines+markers",
          marker: { color: "red", size: 5 },
        };
        let trace_price_4th = {
          name: "Price Wonderland($)",
          x: dataWonderland.index.map((t) => new Date(t)),
          y: dataWonderland.price,
          xaxis: "x",
          yaxis: "y1",
          type: "scatter",
          mode: "lines+markers",
          marker: { color: "green", size: 5 },
        };
        let trace_price_5th = {
          name: "Price Snowdog($)",
          x: dataSnowdog.index.map((t) => new Date(t)),
          y: dataSnowdog.price,
          xaxis: "x",
          yaxis: "y1",
          type: "scatter",
          mode: "lines+markers",
          marker: { color: "yellow", size: 5 },
        };
        let trace_price_6th = {
          name: "Price Avax($)",
          x: dataAvax.index.map((t) => new Date(t)),
          y: dataAvax.price,
          xaxis: "x",
          yaxis: "y1",
          type: "scatter",
          mode: "lines+markers",
          marker: { color: "white", size: 5 },
        };
        let trace_volumes = {
          name: "Volumne ($B)",
          x: data.index.map((t) => new Date(t)),
          y: data.volumes,
          xaxis: "x",
          yaxis: "y2",
          type: "bar",
          barmode: "relative",
          marker: {
            color: "rgb(49,130,189)",
            opacity: 0.7,
          },
        };
        let trace_volumes_2nd = {
          name: "Volumne ($B)",
          x: dataHector.index.map((t) => new Date(t)),
          y: dataHector.volumes,
          xaxis: "x",
          yaxis: "y2",
          type: "bar",
          barmode: "relative",
          marker: {
            color: "rgb(49,130,189)",
            opacity: 0.7,
          },
        };
        let layout = {
          autosize: true,
          height: "100%",
          margin: {
            l: 50,
            r: 20,
            t: 35,
            pad: 3,
          },
          plot_bgcolor: "transparent",
          paper_bgcolor: "transparent",

          showlegend: true,
          legend: {
            x: 1,
            y: 0,
          },
          xaxis: {
            domain: [1, 1],
            anchor: "y2",
          },
          yaxis: {
            domain: [0.1, 1],
            anchor: "x",
          },
          yaxis2: {
            showticklabels: false,
            domain: [0, 0.1],
            anchor: "x",
          },
          grid: {
            roworder: "bottom to top",
          },
        };
        let config = { responsive: true };
        let series = [
          trace_price,
          trace_price_2nd,
          trace_price_3rd,
          trace_price_4th,
          trace_price_5th,
          trace_price_6th,
        ];
        Plotly.newPlot("chart", series, layout, config);
      };

      initChart(
        historyData,
        historyHectorData,
        historySnowData,
        historyWonderlandData,
        historySnowdogData,
        historyAvaxData
      );

      let j = 0;
      for (var k in coinsData) {
        for (var l = 0; l < data.length; l++) {
          if (data[l].api === k) {
            data[l].price = coinsData[k].usd;
            j++;
          }
        }
      }
      setData([...data]);
    };
    wrapper();
    let inv = 2500;
    let roiii = 0.6154;
    let discount = 20;
    let dayys = 5;
    mintProfit(inv, roiii, discount, dayys);
    profitCalculator(inv, roiii, 24 * dayys);
  }, []);

  const mintProfit = (invest, r_8_hour, mint_discount, locked_time_in_days) => {
    const investAfterDiscount = invest * (1 + mint_discount / 100);
    const rebases = locked_time_in_days * 3;
    const rPerClock = investAfterDiscount / rebases;
    let profit = 0;
    for (var i = 0; i < rebases; i++) {
      profit = (profit + rPerClock) * (1 + r_8_hour / 100);
    }
    console.log("Mint:", profit);
  };

  return (
    <main className="relative">
      <span id="what" className="absolute -top-20"></span>
      <div className="main-padding flex flex-col items-center font-main-regular">
        <p className="text-secondary font-bold uppercase italic text-[2rem] c6:text-[3rem]">
          Calculate me
        </p>
        <div className="w-full flex flex-col justify-center items-center">
          <div className="w-full my-10 relative">
            <input
              placeholder="Your invest in $"
              className="rounded-md py-2 px-4 border-secondary w-full border bg-[#dfdfdf]"
              id="invest"
              name="invest"
              type="number"
              onChange={handleChange}
              value={toSend.invest}
            />
            <div className="py-2 px-4 bg-secondary w-11 absolute top-[1px] right-[1px] rounded-r-md font-bold">
              $
            </div>
          </div>
          {data.map((item, i) => (
            <div
              key={i}
              className="flex flex-row font-main-regular text-white  w-full items-center 
                        justify-center pb-5 border-b border-dotted border-secondary"
            >
              <div className="flex flex-col">
                <div className="flex flex-col justify-start items-start">
                  <p className="px-3 py-2 w-full text-left my-1 font-bold">
                    {item.name}
                  </p>
                  <div
                    className="relative rounded-full w-[50px] h-[50px] bg-center bg-cover cursor-pointer ml-2"
                    style={{ backgroundImage: "url(" + item.img + ")" }}
                  ></div>
                </div>
                <p className="px-3 py-2 w-full my-1">
                  Act. price: {item.price} {item.currency}
                </p>
                <p className="px-3 py-2 w-full my-1">
                  Yield per 8h: {item.roi_1day}
                </p>
              </div>
              <div className="flex flex-col my-3 px-3 py-2 w-2/3 text-center">
                <div className="flex flex-row  ">
                  <p className=" border border-secondary py-3 w-1/5">Time:</p>
                  <p className=" border border-secondary py-3 w-1/5">1 Day</p>
                  <p className=" border border-secondary py-3 w-1/5">1 Week</p>
                  <p className=" border border-secondary py-3 w-1/5">1 Month</p>
                  <p className=" border border-secondary py-3 w-1/5">1 Year</p>
                </div>
                <div className="flex flex-row">
                  <p className=" border border-secondary py-3 w-1/5">Profit:</p>
                  <p className=" border border-secondary py-3 w-1/5">
                    {profit[i].profit_day} $
                  </p>
                  <p className=" border border-secondary py-3 w-1/5">
                    {profit[i].profit_week} $
                  </p>
                  <p className=" border border-secondary py-3 w-1/5">
                    {profit[i].profit_month} $
                  </p>
                  <p className=" border border-secondary py-3 w-1/5">
                    {profit[i].profit_year} $
                  </p>
                </div>
                <div className="flex flex-row">
                  <p className=" border border-secondary py-3 w-1/5">
                    Lamborghini Huracan:
                  </p>
                  <p className=" border border-secondary py-3 items-center flex justify-center w-4/5">
                    214.415,00 $
                  </p>
                </div>
              </div>
            </div>
          ))}
        </div>

        {/* <a href="/#why" className="flex flex-col items-center">
          <p className="text-secondary text-[3rem] relative top-20 bounce-arrow">
            <HiOutlineArrowNarrowDown />
          </p>
          <p className="mx-4 font-light text-secondary relative top-28 text-[1.5rem] blink-text">
            WHY
          </p>
        </a> */}
      </div>
      {isLoading ? (
        <h6 className="value animate__animated animate__flash animate__slow text-center text-primary">
          {" "}
          loading ...
        </h6>
      ) : (
        <>
          <h2
            id="last-price"
            className="text-center text-primary animate__animated"
          ></h2>
          <div id="chart" className="p-0 m-0 max-w-[80vw] mx-auto"></div>
        </>
      )}
      <div className="flex flex-row items-center justify-center w-full pt-32 pb-10">
        <p className="text-center text-white">All rights reserved &copy; </p>
        <a href="/">
          <p className="ml-2 tracking-[-0.2rem] c4:text-[1.9rem] font-black text-secondary">
            STAKEBROS.
          </p>
        </a>
      </div>
    </main>
  );
}
