import { HiOutlineArrowNarrowDown } from "react-icons/hi";
import gif from "../assets/Bitcoin-Factory-Crypto-Gif.gif";
import Image from "next/image";

export default function Who() {
  return (
    <main className="relative">
      <span id="who" className="absolute top-20"></span>
      <div className="mt-20 main-padding flex flex-col items-center font-main-regular">
        <p className="text-secondary font-thin italic text-[2rem] mt-40 c6:text-[3rem]">
          WHO we are?
        </p>
        <p className="text-white  text-justify my-5 font-light c6:text-[1.5rem]">
          <strong className="font-bold">Ja wer sind wir eigentlich?: </strong>
          Kurz gesagt sind wir ein paar stino Typen die kein Bock haben Ihre
          Kohle auf irgendein Gammeldepot bei der Sparkasse zu legen, weil wir
          der Meinung sind das man an anderen Stellen auf dieser Welt wo der
          Sprit 2€ kostet sein Geld anlegen kann. Sicher ist diese Option nicht
          so safe wie Aktien, wobei auch die mal flöten gehen können wenn Elon
          mal wieder Lust hat paar spuckige Posts zu machen, aber Ihr wisst was
          ich meine. Wer bock hat meldet sich per WhatsApp, meine Nummer habt
          Ihr oder Ihr meldet euch per Kontaktformular, dann sprechen wir über
          mögliche Dinge und können Fragen besprechen.
        </p>

        <a
          href="/contact"
          className="text-center bg-secondary rounded px-4 py-3 mt-10 mb-60 font-bold 
                  md:mb-[600px] xl:mb-[750px] c4:mb-[850px] c5:mb-[1050px] c6:mb-[1300px]"
        >
          Echt noch Fragen?!
        </a>
      </div>
      <div className="blendi imageContainer w-full mx-auto absolute bottom-0">
        <Image
          src={gif}
          alt="sofortüberweisung.icon"
          layout="fill"
          className="image_"
        />
      </div>
    </main>
  );
}
