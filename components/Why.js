import { HiOutlineArrowNarrowDown } from "react-icons/hi";

export default function Why() {
  return (
    <main className="relative">
      <span id="why" className="absolute top-20"></span>
      <div className="mt-20 main-padding flex flex-col items-center font-main-regular">
        <p className="text-secondary font-thin italic text-[2rem] mt-40 c6:text-[3rem]">
          WHY we do this shit?
        </p>
        <p className="text-white  text-justify my-5 font-light c6:text-[1.5rem]">
          <strong className="font-bold">Das ist ganz einfach: </strong>
          Wir nehmen an du kratz Dir 2.000€ zusammen und gehst als Einmann-Armee
          in die Lotterie, deine Chance hier etwas zu gewinnen ist maginal. Nun
          nehmen wir an du bist in einem zusammenschluss aus 10 Personen welche
          durch 20.000€ in der Lotterie vertreten sind. Dann ist deine Chance um
          das 9 Fache gestiegen. Natürlich sinkt auch dein Gewinn um das 9
          Fache, jedoch gewinne ich lieber 1.200€ von den möglichen 12.000€ (bei
          einem Ticket) als 0€. Es seidenn Du bist ein Glückspilz lol, dann geh
          weg B==D !
        </p>

        <a href="/#who" className="flex flex-col items-center">
          <p className="text-secondary text-[3rem] relative top-20 bounce-arrow">
            <HiOutlineArrowNarrowDown />
          </p>
          <p className="mx-4 font-light text-secondary relative top-28 text-[1.5rem] blink-text">
            WHO
          </p>
        </a>
      </div>
    </main>
  );
}
