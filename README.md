# Web-Application - Customer Name

This repository includes the website code of the development for <CustomerName>.

## Content

This webapplication is build with React, Next.JS, TailwindCSS and typicall HTML5.

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install all required dependencies for this project.

```bash
npm install
```

## Usage

To enter de development mode you could execute the following command:

```bash
npm run dev
```

This command will start a preview of the website in developer mode.

## Build

To build a production build of this application execute the following command:

```bash
npm run build
```

## Contributing

Pull requests are not allowed. For major changes, please open an issue first contact us via e-mail at: [info@ambitive.de](mailto:info@ambitive.de).

## License

All Rights reserved Härdrich & Meister GbR / Ambitive - Digitalagentur.
