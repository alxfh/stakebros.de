import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <Html lang="de">
        <Head>
          <meta
            name="CustomerName"
            content="CustomerName NextJS Web-Applikation"
          />
          <meta name="google-site-verification" content="XXXXXXXXXXXXX" />
          <meta name="facebook-domain-verification" content="XXXXXXXXXXX" />
          <link rel="apple-touch-icon" href="apple-touch-icon.png" />
          <link rel="icon" href="/favicon.png" />
          {/* LOAD GOOGLE FONT */}
          <link rel="dns-prefetch" href="https://fonts.googleapis.com" />

          <link
            href="https://fonts.googleapis.com/css2?family=Exo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=IBM+Plex+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&family=Roboto:wght@100;300;400;500;700;900&display=swap"
            rel="stylesheet"
          />
          <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
