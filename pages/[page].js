import Head from "next/head";
import dynamic from "next/dynamic";
import data from "../model/Model.json";
import { useRouter } from "next/router";

const comps = data.componentList;
const compList = [];
const STORAGE_URL = data.main_config.storage_url;

for (var i = 0; i < comps.length; i++) {
  let compName = comps[i].name;
  compList["" + compName + ""] = {
    component: dynamic(() => import("../components/" + compName)),
  };
}

export default function Page() {
  //get Data
  const router = useRouter();
  const pageName = router.query.page;
  let page = { components: [] };
  let pageStack = [];
  pageStack = data.pages;

  //add subpages to main pages
  for (var i = 0; i < data.sub_pages.length; i++) {
    pageStack.push(data.sub_pages[i]);
  }

  //set content
  for (var i = 0; i < pageStack.length; i++) {
    if (
      pageName === data.pages[i].href.replace("/", "") ||
      (pageName === undefined && data.pages[i].href === "/")
    ) {
      for (var j = 0; j < data.pages[i].components.length; j++) {
        data.pages[i].components[j].render =
          compList["" + data.pages[i].components[j].name + ""].component;
      }
      page = data.pages[i];
    }
  }

  return (
    <div className="App">
      <Head>
        <title>{page.page_title}</title>
        <meta name="description" content={page.meta_description} />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/*<PasswordMask />*/}
      {page.components.map((item, index) => (
        <div key={index}>
          <item.render
            data={item.props}
            pos={index}
            STORAGE_URL={STORAGE_URL}
          />
        </div>
      ))}
    </div>
  );
}
