import Head from "next/head";
import dynamic from "next/dynamic";
import data from "../model/Model.json";
import { useState, useEffect } from "react";

const compList = [];
const MAX_RENDERED_ITEMS = 3;
const STORAGE_URL = data.main_config.storage_url;

for (var i = 0; i < data.pages[0].components.length; i++) {
  let compName = data.pages[0].components[i].name;
  if (i === 0) {
    compList["" + compName + ""] = {
      component: require("../components/" + compName).default,
    };
  } else {
    compList["" + compName + ""] = {
      component: dynamic(() => import("../components/" + compName)),
    };
  }
}

export default function Page() {
  const [load, loadContent] = useState(false);

  const handleScroll = function (event) {
    loadContent(true);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);
  //get Data
  let page = { components: [] };

  //set content
  for (var j = 0; j < data.pages[0].components.length; j++) {
    data.pages[0].components[j].render =
      compList["" + data.pages[0].components[j].name + ""].component;
  }
  page = data.pages[0];

  return (
    <div className="App">
      <Head>
        <title>{page.page_title}</title>
        <meta name="description" content={page.meta_description} />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {page.components.map((item, index) => (
        <div key={index}>
          {(load || index <= MAX_RENDERED_ITEMS) && (
            <item.render
              data={item.props}
              pos={index}
              STORAGE_URL={STORAGE_URL}
            />
          )}
        </div>
      ))}
    </div>
  );
}
