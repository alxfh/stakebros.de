import "../styles/globals.css";
import CookieConsent from "../components/CookieConsent";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Component {...pageProps} />
      <CookieConsent />
    </>
  );
}

export default MyApp;
